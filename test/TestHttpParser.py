import unittest, json, subprocess, os, random

class TestHttpParser(unittest.TestCase):

    def execute(self, stdin_data):
        args = ["{0}/bin/http_parser".format(os.getcwd())]
        p = subprocess.Popen(args,
                stdin=subprocess.PIPE,
                stderr=subprocess.PIPE,
                stdout=subprocess.PIPE)
        return p.communicate(stdin_data)

    def test_error_on_invalid_data(self):
        (result, stderr) = self.execute("HAHA")
        # print stderr
        parsed = json.loads(result)
        self.assertTrue(parsed["error"], "#FIXME")


    def test_basic_api(self):
        method = random.choice(["GET", "HEAD"])
        query_value = random.random()
        curl_version = random.randint(1, 10)
        port = random.randint(3000, 5000)

        words = ["W", "o", "r", "l", "d"]
        random.shuffle(words)
        body = "".join(words)

        headers = []
        headers.append("{0} /test?ok={1} HTTP/1.1\r\n".format(method, query_value))
        headers.append("User-Agent: curl/{0}\r\n".format(curl_version))
        headers.append("Host: 0.0.0.0:{0}\r\n".format(port))
        headers.append("Accept: */*\r\n")
        headers.append("Content-Length: 5\r\n")
        headers.append("\r\n")
        headers.append(body)

        http_request = "".join(headers)
        (result, stderr) = self.execute(http_request)
        parsed = json.loads(result)

        self.assertEqual("curl/{0}".format(curl_version), parsed["User-Agent"])
        # self.assertEqual("5", parsed["Content-Length"])
        self.assertEqual(method, parsed["method"])
        self.assertEqual("0.0.0.0:{0}".format(port), parsed["Host"])
        self.assertEqual("*/*", parsed["Accept"])
        self.assertEqual("/test?ok={0}".format(query_value), parsed["url"])
        self.assertEqual("1.1", parsed["protocol"])
        self.assertEqual(body, parsed["body"])


if __name__ == '__main__':
    unittest.main()
